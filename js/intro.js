let obj1 = `
{
    "message": {
        "from": {
            "name": "Carlos",
            "mail": "cahucadi@gmail.com"
        },
        "to": {
            "name": "Diana",
            "mail": "dianasolano@gmail.com"
        },
        "content": {
            "subject": "JS Example",
            "text": "Content"
        }
    }
}
`;

let result1 = JSON.parse( obj1 );

console.log( result1.message.from.name );

let obj2 ={
    message: {
        from: {
            name: "Carlos",
            mail: "cahucadi@gmail.com"
        },
        to: {
            name: "Diana",
            mail: "dianasolano@gmail.com"
        },
        content: {
            subject: "JS Example",
            text: "Content"
        }
    }
};

let result2 = JSON.stringify( obj2 );

console.log( result2 );
